﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MiPrimerWebServiceBD
{
    public class PersonaBL
    {

        public Response AgregarPersona(Persona p)
        {
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=empresa;" +
                "Integrated Security=True;";
            SqlConnection cnn = new SqlConnection(connectionString);
            try
            {
                string query = "INSERT INTO Persona(Nombre, Apellido, Direccion, FechaNacimiento, Telefono)" +
                    "VALUES(@Nombre, @Apellido, @Direccion, @FechaNacimiento, @Telefono)";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                cmd.Parameters.Add("@Nombre", SqlDbType.VarChar, 50).Value = p.Nombre;
                cmd.Parameters.Add("@Apellido", SqlDbType.VarChar, 50).Value = p.Apellido;
                cmd.Parameters.Add("@Direccion", SqlDbType.VarChar, 300).Value = p.Direccion;
                cmd.Parameters.Add("@FechaNacimiento", SqlDbType.Date).Value = p.FechaNacimiento.Date;
                cmd.Parameters.Add("@Telefono", SqlDbType.VarChar, 10).Value = p.Telefono;

                int resultado = cmd.ExecuteNonQuery();

                cnn.Close();

                if (resultado > 0)
                {
                    return new Response(200, "Se agregó la información !");
                }
                else
                {
                    return new Response(400, "No se pudo guardar la información!");
                }
            }
            catch (Exception ex)
            {
                return new Response(500, "No se pudo guardar la información !" + ex.Message);
            }
        }

        public List<Persona> ListarPersonas()
        {
            List<Persona> lista = new List<Persona>();
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=empresa;" +
                "Integrated Security=True;";
            SqlConnection cnn = new SqlConnection(connectionString);
            try
            {
                string query = "SELECT * FROM Persona";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                SqlDataReader resultado = cmd.ExecuteReader();

                while (resultado.Read())
                {
                    Persona p = new Persona()
                    {
                        Id = int.Parse(resultado["Id"].ToString()),
                        Nombre = resultado["Nombre"].ToString(),
                        Apellido = resultado["Apellido"].ToString(),
                        Direccion = resultado["Direccion"].ToString(),
                        FechaNacimiento = Convert.ToDateTime(resultado["FechaNacimiento"].ToString()),
                        Telefono = resultado["Telefono"].ToString(),
                    };
                    lista.Add(p);
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                lista = new List<Persona>();
            }

            return lista;
        }

    }
}