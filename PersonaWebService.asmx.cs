﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace MiPrimerWebServiceBD
{
    /// <summary>
    /// Descripción breve de PersonaWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class PersonaWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public Response AgregarPersona(Persona p)
        {
            Response r = new PersonaBL().AgregarPersona(p);
            return r;
        }

        [WebMethod]
        public List<Persona> ListarPersonas()
        {
            return new PersonaBL().ListarPersonas();
        }
    }
}
