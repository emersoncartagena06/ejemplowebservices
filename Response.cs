﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiPrimerWebServiceBD
{
    public class Response
    {
        public Response()
        {

        }

        public Response(int statusCode, string message)
        {
            this.statusCode = statusCode;
            this.message = message;
        }


        public int statusCode { get; set; }
        public string message { get; set; }
    }
}